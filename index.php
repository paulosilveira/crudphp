﻿<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title>CRUD php</title>
<!-- Bootstrap -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<![endif]-->
</head>
<body>
  <div class="face-login-wrap">
    <div class="face-login-content">
      <div class="fb-login-button" data-max-rows="1" data-size="xlarge" data-show-faces="false" data-auto-logout-link="true"></div>
    </div>
    <div class="btn-menu-mobile">
      <img src="img/btn_menu_mobile.png"/>
    </div>
  </div>
  <div class="menu-mobile-content">
    <ul class="nav nav-pills nav-stacked">
      <li><a target="_blank" href="https://github.com/paulpetone/crudphp">Veja o projeto no GitHub</a></li>
    </ul>
  </div>
  <!-- Modal -->
  <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h3 id="myModalLabel">Atualizar</h3>
    </div>
    <form class="form-up">
      <div class="modal-body">
        ID: <input type="text" class="id-update" name="id" readonly/><br/>
        NOME: <input type="text" class="nome-update" name="nome"/><br/>
        PROJETOS: <input type="text" class="projeto-update" name="projeto"/><br/>
        ANO DE INICIO: <input type="text" class="ano-update" name="anoinicio"/><br/>
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Fechar</button>
        <input type="submit" class="btn btn-primary" value="Salvar">
      </div>
    </form>
  </div>

  <ul class="nav nav-pills menu-crud">
    <li data-menu="cadastrar">
      <a href="#">Cadastrar</a>
    </li>
    <li data-menu="read"><a href="#">Vizualizar</a></li>
    <li data-menu="update"><a href="#">Atualizar</a></li>
    <li data-menu="delete" ><a href="#">Deletar</a></li>
  </ul>

  <article id="content">
  </article>
  <footer></footer>
<!-- jquery e bootstrap js -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://malsup.github.com/jquery.form.js"></script> 
<script src="js/bootstrap.js"></script>
<script src="js/jquery-ui-1.10.4.custom.js"></script>

<!-- FACEBOOK -->
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1488470874702683',
      status     : true,
      cookie     : true,
      xfbml      : true
    });

    FB.Event.subscribe('auth.login', function(response){
      window.setTimeout('location.reload()', 1);
    });
  
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            $('#content').css('display','block');
            sendNameObj();
        } else if (response.status === 'not_authorized') {
            $('#content').css('display','none');
            $('.welcome').text('Você não tem autorização para esse APP');
            $('.menu-crud').css('display','none');
            $("<h2 class='welcome'>Você não tem acesso a esse APP.</h2>").insertAfter($('.face-login-wrap'));
        } else {
            $('#content').css('display','none');
            $('.welcome').text('Faça login para continuar!');
            $('.menu-crud').css('display','none');
            $("<h2 class='welcome'>Faça login para conitnuar.</h2>").insertAfter($('.face-login-wrap'));
        }
    });

    function sendNameObj() {
      FB.api('/me', function(response) {
        obj.insertWelcome(response.name);
      });
    }
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/all.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

<script src="js/main.js"></script>

<!-- END FACEBOOK -->

<script type="text/javascript">
  $('.form-up').submit(function(event){
    event.preventDefault();
    $.ajax({
      type: 'GET',
        url: "ajax/up.php",
          data: $('.form-up').serialize()
      })
      .fail(function(res){
        alert('fail');
      })
      .success(function(res){
        console.log(res);
        $('.modal-header .close').click();
        $($('.menu-crud li')[2]).click();
        alert('Dados Cadastrados Com Sucesso');
      });
  });
</script>

</body>
</html>
