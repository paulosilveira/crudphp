CRUDPHP
=======
Crud simples em php, responsivo, usando:

PHP, AJAX, JQUERY, BOOTSTRAP

Sql's para rodar local:

CREATE SCHEMA `crud_php` ;

CREATE TABLE `crud_php`.`clientes` (
  `idclientes` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL,
  `projetos` VARCHAR(45) NULL,
  `anoinicio` INT NULL,
  PRIMARY KEY (`idclientes`));
