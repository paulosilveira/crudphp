var obj = {
    init : function(){
        obj.bind();
    },
    bind : function(){
        $('.menu-crud li').click(obj.menuActive);
        $('.btn-menu-mobile').click(obj.openMenuMobile);
    },
    menuActive : function(event){
        $('.menu-crud li').removeClass('active');
        var el = $(event.currentTarget);
        el.addClass('active');
        obj.loadPages(el); 
        if(!$('.menu-crud li').hasClass('active')){
           $("<h2 class='welcome'>Escolha uma opção!</h2>").insertBefore($('.menu-crud'));
        } else {
            $('.welcome').remove();
        }
    },
    insertWelcome : function(nome){
       $("<h2 class='welcome'>Bem-vindo, "+nome+"!<br/>Escolha uma opção.</h2>").insertBefore($('.menu-crud')); 
    },
    loadPages : function(el){
        var str = $(el).attr('data-menu');
        $("#content").load("ajax/"+str+".php");
    },
    toModal : function(event){
        var id = event.currentTarget.parentElement.parentElement.childNodes[1].innerText;
        var nome = event.currentTarget.parentElement.parentElement.childNodes[3].innerText;
        var projeto = event.currentTarget.parentElement.parentElement.childNodes[5].innerText;
        var ano = event.currentTarget.parentElement.parentElement.childNodes[7].innerText;
        $('.id-update').val(id);
        $('.nome-update').val(nome);
        $('.projeto-update').val(projeto);
        $('.ano-update').val(ano);
    },
    animateUl : function(event){
        var ul = $(event.currentTarget.parentElement.parentElement);
        var stage = $(window).width();
        var tableUls = $('.table-db');
        var dbContent = $('.db-content');
        ul.animate({
            'margin-left' : '-' + stage + 'px'
        }, '2000');
        setTimeout(function() {
            ul.css('display', 'none');
        },500);
        if(dbContent.children().length != 6 && !$(ul).is(':last-child')){
            setTimeout(function() {
                tableUls.effect( "shake" );
            },500);
        }
    },
    openMenuMobile : function(){
        var menu = $('.menu-mobile-content');

        if(!menu.hasClass('ativo')){
            menu.css('display','block');
            menu.addClass('ativo');
        } else {
            menu.css('display','none');
            menu.removeClass('ativo');
        }
        
    }
}
$(document).ready(obj.init);
