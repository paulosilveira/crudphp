<h1>CADASTRAR</h1>
<form id="form-cad" method="post">
    <p>Nome:</p>
    <input type="text" name="nome"/><br/>
    <p>Projeto:</p>
    <input type="text" name="projeto"/><br/>
    <p>Ano de Inicio:</p>
    <input type="text" name="anoinicio"/><br/>
    <input type="submit" name="go" class="btn btn-primary" value="Cadastrar!"/>
</form>
<script type="text/javascript">
	$('#form-cad').submit(function(event){
		event.preventDefault();
		$.ajax({
			type: 'POST',
	  		url: "ajax/insert.php",
		  		data: $( "#form-cad" ).serialize()
			})
			.fail(function(res){
				alert('fail');
			})
			.success(function(res){
				$($('.menu-crud li')[0]).click();
				alert('Dados Cadastrados Com Sucesso');
			});
	});
</script>