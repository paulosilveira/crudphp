<?php
    require_once "../backend/dbcon.class.php";
    $con = new dbcon();
    $con->connect();
?>
<h1>DELETAR</h1>

<div class="table-db">
	<ul class="db-ul db-ul-header">
		<li>
			<b>ID</b>
		</li>
		<li>
			<b>NOME</b>
		</li>
		<li>
			<b>PROJETO</b>
		</li>
		<li>
			<b>ANO DE INICIO</b>
		</li>
	</ul>
	<div>
		<?php
			$consulta = mysql_query("SELECT * FROM clientes");
			while($campo = mysql_fetch_array($consulta)){
		?>
		<ul class="db-ul db-content">
			<li>
				<?php echo $campo['idclientes'];?>
			</li>
			<li>
				<?php echo $campo['nome'];?>
			</li>
			<li>
				<?php echo $campo['projetos'];?>
			</li>
			<li>
				<?php echo $campo['anoinicio'];?>
			</li>
			<li class="li-form-del">
				<form class="form-del-<?php echo $campo['idclientes'];?> form-del" method="get">
	                <input type="submit" class="btn btn-danger" name="del" value="Deletar"/>
	                <input type="hidden" name="id" class="hide-input" value="<?php echo $campo['idclientes'];?>">
                </form>
            </li>
            <div class="clear"></div>
		</ul>
		<?php } ?>
	</div>
</div>

<script type="text/javascript">

	$('.form-del').submit(function(event){
		event.preventDefault();
		obj.animateUl(event);
		var num = $(event.currentTarget[1]).val();
		$.ajax({
			type: 'GET',
	  		url: "ajax/del.php",
		  		data: $('.form-del-'+num).serialize()
			})
			.fail(function(res){
				alert('fail');
			})
			.success(function(res){
				setTimeout(function() {
					$($('.menu-crud li')[3]).click();
					alert('Dados Deletados Com Sucesso');
				}, 1000);
			});
	});
</script>