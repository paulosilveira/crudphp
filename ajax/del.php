<?php
    require_once "../backend/dbcon.class.php";
    require_once "../backend/crud.class.php"; 

	$con = new dbcon();
	$con->connect();
	$crud = new crud();
	$id = $_GET['id'];
	
	$content = json_encode($id);
	echo $content;
	
	$crud->delete($id);
?>